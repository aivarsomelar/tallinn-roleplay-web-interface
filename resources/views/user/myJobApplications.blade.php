@extends('user.layout.layout')

@section('userBody')
    <div class="card mt-5">
        @foreach($applications as $application)
            <div class="card-header"><b>{{__('headers.'.$application->job)}} {{__('headers.application')}}
                    #{{$application->id}}</b></div>
            <div class="card-body">
                <a class="border-light border-bottom row nav-link disabled" data-toggle="collapse"
                   href="#multiCollapse{{$application->id}}" aria-expanded="false"
                   aria-controls="multiCollapseExample1">
                    <strong>{{__('texts.status')}}: </strong>
                    @if($application->isAccepted and !$application->initiated)
                        <span class="badge-success pl-2 pr-2"><strong
                                class="text-uppercase">{{__('texts.accepted')}}</strong></span>
                    @endif
                    @if($application->initiated and $application->isAccepted)
                        <span class="badge-success pl-2 pr-2"><strong
                                class="text-uppercase">{{__('texts.hired')}}</strong></span>
                    @endif
                    @if($application->isRejected)
                        <span class="badge-danger pl-2 pr-2"><strong
                                class="text-uppercase">{{__('texts.rejected')}}</strong></span>
                    @endif
                    @if(!$application->isRejected and !$application->isAccepted)
                        <span class="badge-secondary pl-2 pr-2"><strong
                                class="text-uppercase">{{__('texts.pending')}}</strong></span>
                    @endif
                </a>
                @if($application->isAccepted and !$application->initiated)
                    <p class="mt-3">
                        {{__('texts.contact_with_director_of_the_establishment_to_set_up_a_meeting')}}
                    </p>
                @endif
                <div class="mt-3 collapse multi-collapse" id="multiCollapse{{$application->id}}">
                    @foreach(json_decode($application->answers) as $key => $answer)
                        <p>
                            <strong>{{ __('jobApplications.' . $key) }}:</strong>
                        </p>
                        <p>
                            {{$answer}}
                        </p>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
@endsection
