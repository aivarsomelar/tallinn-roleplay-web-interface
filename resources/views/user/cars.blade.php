@extends('user.layout.layout')

@section('userBody')
    <div class="card mt-5">
        @component('user.components.cars', ['cars' => $cars, 'carSellRequests' => $carSellRequests, 'userData' => $userData])
        @endcomponent
    </div>
@endsection
<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmSell() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }

</script>
