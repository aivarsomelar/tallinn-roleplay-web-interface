<table class="table">
    <thead class="thead-light">
    <tr>
        <th scope="col">{{__('tables.request_nr')}}</th>
        <th scope="col">{{__('tables.number_plate')}}</th>
        <th scope="col">{{__('tables.seller')}}</th>
        <th scope="col">{{__('tables.buyer')}}</th>
        @if($history)
            <th scope="col">{{__('tables.approver')}}</th>
        @endif
        <th scope="col">{{__('tables.actions')}}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($sellRequests as $sellRequest)
        <tr>
            <th scope="row">{{$sellRequest->id}}</th>
            <td>{{$sellRequest->plate}}</td>
            <td>{{\App\Helpers\UserHelper::getCharacterName(\App\User::find($sellRequest->sellerId))}}</td>
            <td>{{\App\Helpers\UserHelper::getCharacterName(\App\User::find($sellRequest->buyerId))}}</td>
            @if($history)
                <td>{{$sellRequest->confirmedBy}}</td>
            @endif
            <td>
                <a class="btn btn-info" href="{{route('usedCarsSellRequest', ['sellRequestId' => $sellRequest->id])}}"
                   onclick="overlayOn()">
                    {{__('buttons.open')}}
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
