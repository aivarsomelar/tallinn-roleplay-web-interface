@component('components.box')
    <div class="row">
        <div class="col-md-12 text-center mt-4 mb-1"><strong>{{$companyName}}</strong></div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center mt-1 mb-4">
            <i class="far fa-money-bill-alt fa-3x text-success text-center"></i>
            <div class="mt-0 align-middle font-weight-bold" style="color: #1b1e21; font-size: 20px;">{{$companyMoney}}</div>
        </div>
    </div>
@endcomponent
