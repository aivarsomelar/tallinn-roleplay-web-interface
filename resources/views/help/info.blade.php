@extends('help.layout.layout')
@section('helpHeader')
    <strong>Üldine info</strong>
@endsection
@section('helpBody')
    <div>
        <h4><strong>Key binds / Nupud</strong></h4>
        <div class="row">
            <div class="col-md-4">
                <ul>
                    <li><strong>F1</strong> - Põhi menüü mille kaudu saab avada telefoni, inventuuri jne</li>
                    <li><strong>F2</strong> - Lemmik animatsioon (ennem tuleb lemmik animatsioon valida)</li>
                    <li><strong>F2</strong> - Sulgeb inventuuri kui see on avatud</li>
                    <li><strong>F3</strong> - Animatsioonid</li>
                    <li><strong>F6</strong> - Töö menüü</li>
                    <li><strong>F9</strong> - Lemmiklooma menüü</li>
                    <li><strong>F12</strong> - Pildistamine (steami funktsionaalsus)</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><strong>E</strong> - Vilkurid (politsei, kiirabi ja mehaanikute sõidukitel)</li>
                    <li><strong>T</strong> - Text chat</li>
                    <li><strong>Y</strong> - Püsikiiruse hoidja</li>
                    <li><strong>G</strong> - Sireenid (politsei ja kiirabi sõidukitel)</li>
                    <li><strong>H</strong> - Hääle/kuulmise kauguse muutmine.</li>
                    <li><strong>Z</strong> - Turvavöö</li>
                    <li><strong>X</strong> - Käed ülesse</li>
                    <li><strong>C</strong> - Selja taha vaatamine ja narkootikumide müümine</li>
                    <li><strong>B</strong> - Näita näpuga</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><strong>NUM 7</strong>Röövimise menüü</li>
                    <li><strong>NUM 9</strong>Relva menüü</li>
                    <li><strong>SHIFT + L</strong> - Käed raudu koos animatsiooniga (ainult politseil)</li>
                    <li><strong>SHIFT + M</strong> - Maski pähe panek/ära võtmine</li>
                    <li><strong>HOME</strong> - Mängijate nimekiri</li>
                    <li><strong>CTRL</strong> - kükitamine</li>
                    <li><strong>CTRL + M</strong> - Kiiruse radar (politseil)</li>
                    <li><strong>CAPS LOCK</strong> - Raadio Saatjaga ja telefoniga rääkimine (push to talk)</li>
                    <li><strong>ESC / backspace</strong> - Menüüde sulgemine</li>
                    <li><strong>DELETE</strong> - Käivitab/Lõpetab töö missiooni (kiirabi, mehaanik)</li>
                    <li><strong>ALT</strong> - Lõpetab animtsiooni</li>
                    <li><strong>PAGE DOWN</strong> - Avab Autode extrate menüü</li>
                </ul>
            </div>
        </div>
        <h4><strong>Commandid / Käsud</strong></h4>
        <div class="row">
            <div class="col-md-4">
                <ul>
                    <li><strong>/anontweet</strong> - Kirjuta anonüümne tweet (maksab raha)</li>
                    <li><strong>/reklaam</strong> - Reklaami toodet, üritust, teenust, jne. (maksab raha)</li>
                    <li><strong>/kaebus</strong> - Võta ühendust adminitega ja räägi ära oma probleem</li>
                    <li><strong>/e {emote nimi}</strong> - Käivita emote (sama mis F3 alt)</li>
                    <li><strong>/rooli</strong> - Istub kõrvalt istmelt rooli</li>
                    <li><strong>/paranda</strong> - Paranda väiksed vead aujutiselt. Kui oled mehaanik 24/7 töökojas, siis parandab auto täielikult (raha eest)</li>
                    <li><strong>/minuid</strong> - Vaata enda ID-d</li>
                    <li><strong>/selga</strong> - Võta isik selga (ära kasuta surnud isiku peal)</li>
                    <li><strong>/tassi</strong> - Tassi isikut (üle õla)</li>
                    <li><strong>/drink</strong> - Tekitab joobes oleku</li>
                    <li><strong>/sober</strong> - Tühistab joobes oleku</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><strong>/mootor</strong> - Käivita auto mootor</li>
                    <li><strong>/telefon</strong> - Võta telefoni välja</li>
                    <li><strong>/minuinventar</strong> - Ava inventuur</li>
                    <li><strong>/autoinventar</strong> - Ava auto inventuur</li>
                    <li><strong>/arved</strong> - Ava arvete menüü</li>
                    <li><strong>/tlnmenu</strong> - Ava isikuandmete, relvade, aktsesuaaride, firma juhtimise jne menüü</li>
                    <li><strong>/tab</strong> - Ava tahvel arvuti</li>
                    <li><strong>/pagass</strong> - Ava/sulge auto pagass</li>
                    <li><strong>/kapott</strong> - Ava/sulge auto kapott</li>
                    <li><strong>/uks1-4</strong> - Ava/sulge auto uks</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><strong>/politseikoer</strong> - kutsub politsei koera (ainult politseil)</li>
                    <li><strong>/gsr {id}</strong> - Tee püssirohu test (ainult politseil)</li>
                    <li><strong>/jail {id} {aeg} "{Põhjus}"</strong> - Saada isik vanig (ainult politseil)</li>
                    <li><strong>/unjail {id}</strong> - Tühista isiku vangla karistus (ainult politseil)</li>
                    <li><strong>/comserv {id} {tööde hulk}</strong> - Saada isik ÜKT-d tegema (ainult politseil)</li>
                    <li><strong>/endcomserv {id}</strong> - Tühista ÜKT (ainult politseil)</li>
                    <li><strong>/med {id}</strong> - Näitab inimese meditsiinilist seisundit (ainult kiirabi)</li>
                </ul>
            </div>
        </div><h5><strong>Race Commands / Ralli Käsud</strong></h5>
        <div class="row">
            <div class="col-md-4">
                <ul>

                    <li><strong>/race start {raha summa} {viivitus alguseni}</strong> - Algatab võidusõidu raha eest kasutades waypointi või salvestatud täppe.
                        Viivitus on 30 sekundit by default</li>
                    <li><strong>/race cancel</strong> - Katkestab võidusõidu ja annab osalejatele raha tagasi</li>
                    <li><strong>/race leave</strong> - Loobu võidusõidust, osavõtu raha tagasi ei saa</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><strong>/race record</strong> - Salvesta kaardipeal (ESC) võidusõidu punktid mis tuleb läbida
                        Klõpsa kakskorda mouse-1, et olemasolev checkpoint üle kirjutada</li>
                    <li><strong>/race clear</strong> -  Kustuta kõik checkpoindid kaardilt</li>
                    <li><strong>/race save {name}</strong> - Salevsta checkpoindid ja anna neile nimetus (Saad tulevikus pre-seti kasutada)</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><strong>/race load {name}</strong> - Lae eelnevalt salvestatud võidusõit</li>
                    <li><strong>/race delete {name}</strong> - Kustuta eelnevalt salvestatud võidusõidu pre-set</li>
                    <li><strong>/race list</strong> - Saad vaadata oma salvestatud võidusõitude pre-set listi</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
