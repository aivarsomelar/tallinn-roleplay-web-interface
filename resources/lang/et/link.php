<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Links
    |--------------------------------------------------------------------------
    |
    | This file contains translations of links
    |
    */

    'home' => 'Minu andmed',
    'help' => 'Reeglid ja info',
    'job_applications' => 'Töö avaldused',
    'accepted_job_applications' => 'Vastu võetud töö avaldused',
    'police_application' => 'Politsei avaldus',
    'ambulance_application' => 'Kiirabi avaldus',
    'police' => 'Politsei',
    'players_list' => 'Mängijate nimekiri',
    'cars_list' => 'Autode nimekiri',
    'reports_list' => 'Raportite nimekiri',
    'fines_list' => 'Trahvide nimekiri',
    'officers_list' => 'Politseinike nimekiri',
    'ambulance' => 'Kiirabi',
    'workers_list' => 'Töötajate nimekiri',
    'car_dealer' => 'ARK',
    'used_cars_sell_request_list' => 'Kasutatud autode müügi avaldused',
    'used_cars_sell_history' => 'Kasutatud autode müügi ajalugu',
    'all_available_vehicles_marks' => 'Kõik saadaval olevad sõidukid',
    'all_owned_cars' => 'Kõik omantatud autod',
    'admin' => 'Admin',
    'all_users' => 'Kõik kasutajad',
    'whitelist_applications' => 'Whitelist-i avaldused',
    'ck_candidates' => 'CK kandidaadid',
    'logout' => 'Logi välja',
    'login' => 'Logi sisse',
    'whitelist_application' => 'Whitelist-i avaldus',
    'my_data' => 'Minu andmed',
    'my_car' => 'Minu autod',
    'criminal_records' => 'Karistusregister',
    'medical_records' => 'Meditsiinilised andmed',
    'my_Job_applications' => 'Minu töö avaldused',
    'user_data' => 'Kasutaja andmed',
    'user_cars' => 'Kasutaja autod',
    'forgot_your_password' => 'Unustasid parooli?',
    'server_rules' => 'Serveri reeglid',
    'police_rules' => 'Politsei reeglid',
    'ems_rules' => 'Kiirabi reeglid',
    'mechanic' => 'Mehaanik',
    'new_report' => 'Uus sissekanne',
    'my_reports' => 'Minu sissekanded',
    'all_mechanic_reports' => 'Kõik sissekanded',
    'server_info' => 'Info',
    'radio' => 'Raadio',
    'bills' => 'Arved',
    'news' => 'Uudised',
    'media' => 'Meedia',
    'legend' => 'Legend',
];
