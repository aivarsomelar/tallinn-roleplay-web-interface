<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCriminalRecordsWithUserRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('criminal_records', function (Blueprint $table) {
            $table->renameColumn('criminal_steam_id', 'user_identifier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('criminal_records', function (Blueprint $table) {
            $table->renameColumn('user_identifier', 'criminal_steam_id');
        });
    }
}
