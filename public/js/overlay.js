function overlayOn() {
    document.getElementById("overlay").style.display = "block";
}

function overlayOff() {
    document.getElementById("overlay").style.display = "none";
}

function pictureOverlayOn($pictureName) {
    document.getElementById("pictureOverlay" + $pictureName).style.display = "block";
}

function pictureOverlayOff($pictureName) {
    document.getElementById("pictureOverlay" + $pictureName).style.display = "none";
}

function showProfileUpload() {
    document.getElementById("profileUpdate").style.display = "block";
}

function hideProfileUpload() {
    document.getElementById("profileUpdate").style.display = "none";
}


function showBillsTable() {
    document.getElementById("billsTable").style.display = "block";
}

function hideBillsTable() {
    document.getElementById("billsTable").style.display = "none";
}

function showLegend() {
    document.getElementById("legendOverlay").style.display = "block";
}

function hideLegend() {
    document.getElementById("legendOverlay").style.display = "none";
}
