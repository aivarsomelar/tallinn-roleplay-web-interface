<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneUserContact extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'phone_users_contacts';
}
